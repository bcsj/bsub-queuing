Suppose you have some code you want to run, say scripts A, B and C, and they need to run in a particular sequence and with certain inputs.

For example
```
A --> [B1, B2, ... BN] --> C
```
Here A is some initialization, B# is N different independent processes, and C is some post processing, which shouldn't run until all B# are done.
Likewise, not B# should start before A has concluded.

We may achive this fairly easily. We will need a job-file for each part. 
Finally, we need a script managing the sequencing, say M.sh; M for manager.

```
M.sh
A.job
B.job
C.job
```

A, B and C are typical job scripts. Nothing special there. 
We will assume B is written as an array-job, thus submitting B.job will automatically spawn all B#.

We will now build the job-manager, which will handle all job-submissions. 
In the end, we will simply submit M.sh to the server and through it the server will submit the remaining jobs on its own.
The crucial thing here is to know how to make one job wait for another. To do this, we will need the job-id of that job.
So we will write the manager to submit jobs and capture their job-ids and then using that for submitting the dependent jobs.

```bash
# Submit job A
MYSTRING=`bsub < A.job`
MYID=`echo $MYSTRING | cut -c 6-12`

# Submit job B
MYSTRING=`bsub -w "ended($MYID)" < B.job`
MYID=`echo $MYSTRING | cut -c 6-12`

# Submit job C
MYSTRING=`bsub -w "ended($MYID)" < C.job`
MYID=`echo $MYSTRING | cut -c 6-12`
```